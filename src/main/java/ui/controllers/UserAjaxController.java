package ui.controllers;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ui.entities.User;
import ui.hibernate.UserDAO;


/**
 * Controller for /users/ urls
 * 
 * @author TalentLab1
 *
 */

@Controller
public class UserAjaxController {
	
	
	Logger logger = Logger.getLogger("uiLogger");
	Utils utl = new Utils();
    
	/**
	 * View returning xhtml with display of the navbar and the div class displayusers
	 * which will be then reloaded by method returnAndShowNext10users
	 * @param userNo
	 * @return
	 */
	//do we need produces 
	@RequestMapping(value="/users/part/{userNo}",method= RequestMethod.GET ,  produces="text/html")
	@ResponseBody
	public ModelAndView show30usersStartingFromFirst(
			@PathVariable("userNo") int userNo,
			ModelAndView mav,
			UserDAO udao
			) {
		mav.setViewName("users/showajaxList");		
		mav.addObject("lastusers", udao.getUsersNumberInDB() );

		System.out.println(" Loading users from = "+userNo);
		List<User> lp = udao.getAllUsersBy30ForPaginationAjaxRequest(userNo, 30);
		mav.addObject("usersList", udao.getUsersNumberInDB() );
		if (lp == null || lp.isEmpty()) {
			System.out.println("null or empty ="+udao.getUsersNumberInDB());
			mav.addObject("userEMPTY","userEMPTY");	
		}
		else {
			System.out.println("not null");
			mav.addObject("usersList",lp);
		}
		return mav;
}
	
	
	/**
	 * Method for returning 10 users and the showajaxList.jsp file with only the 
	 * display of those users, navbar is in showajax.jsp - in which we load the data from below 
	 * @param userNo
	 * @return
	 */
	
	@RequestMapping(value="/users/nextpart/{userNo}",method = RequestMethod.GET ,  produces="text/html")
	@ResponseBody
	public ModelAndView returnAndShowNext30users(
			@PathVariable("userNo") int userNo,
			ModelAndView mav,
			UserDAO udao
			) {
		//LOGIC 
		mav.addObject("lastusers", udao.getUsersNumberInDB() );

		mav.setViewName("users/showajaxList");		
		System.out.println("Loading users from = "+userNo);
		List<User> lp = udao.getAllUsersBy30ForPaginationAjaxRequest(userNo, 30);
		//variables added to MAV
		if (lp == null || lp.isEmpty()) {
			System.out.println("null or empty");
			mav.addObject("userEMPTY","userEMPTY");	
		}
		else {System.out.println("not null");
			mav.addObject("ajaxusersList",lp);
		}
		return mav;
}	
	
	
	/**
	 * Return sorting by names 
	 * @param userNo
	 * @return
	 */
	
	@RequestMapping(value="/users/filterby/{name}",method = RequestMethod.GET )
	@ResponseBody
	public ModelAndView returnFirst30UsersSortedByName(
			@PathVariable("name") String name,
			ModelAndView mav,
			UserDAO udao
			) {
		//LOGIC 
		mav.setViewName("users/showajaxList");		
		List<User> lp = udao.getAllUsersByName(name);
				
		//variables added to MAV
		if (lp == null || lp.isEmpty()) {
			mav.addObject("userEMPTY","userEMPTY");	
		}
		else {
			System.out.println("listForNameFilter count = "+lp.size());
			mav.addObject("usersList",lp);
		}

		return mav;
}
	
	
	/**
	 * Return sorting by names 
	 * @param userNo
	 * @return
	 */
	
	@RequestMapping(value="/users/filterby/{name}/{field}",method = RequestMethod.GET )
	@ResponseBody
	public ModelAndView returnFirst30UsersSortedByNameAndField(
			@PathVariable("name") String name,
			@PathVariable("field") String field,
			ModelAndView mav,
			UserDAO udao
			) {
		mav.setViewName("users/showajaxList");		
		List<User> lp = udao.getAllUsersByNameAndField(name, field);

		if (lp == null || lp.isEmpty()) {
			mav.addObject("userEMPTY","userEMPTY");	
		}
		else {
			System.out.println("listForNameFilter count = "+lp.size());
			mav.addObject("usersList",lp);
		}
		return mav;
}	
	
	
	
	
	//utilities and etc 
	
	/**
	 * Method for casting Long to INT
	 * @param l long
	 * @return int l
	 */
	public int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l +
	          " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	

	
	
	
	
}