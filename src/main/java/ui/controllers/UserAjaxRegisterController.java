package ui.controllers;


import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import ui.entities.User;
import ui.hibernate.UserDAO;



/** 
 * User register Controller.
 * @author TalentLab1
 *
 */

@Controller
public class UserAjaxRegisterController {
	
	
	@RequestMapping(value="/registerUser/", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView registerUser(
			@ModelAttribute("user") User newuser, 
			BindingResult result, 
			SessionStatus status,
			ModelAndView mav
			)
	{		System.out.println("getting the register form");
			mav.setViewName("/register/registerUserForm");
			mav.addObject("user", newuser);
			return mav;
}

			
	//rejestracja usera 
	@RequestMapping(value="/registerUser/", method = RequestMethod.POST)//link url
	@ResponseBody
	public ModelAndView registerUserPOST(
			@ModelAttribute("user") @Valid User newuser, 
			BindingResult result,
			UserDAO udao,
			Utils utils
			) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");//odnosnik do jsp, nie musi sie pokrywac z mapowaniem
	
		if (result.hasErrors()){
		System.out.println(" User add errors ");
		utils.showErrorsForBRObject(result);
		return mav;
		}
		else{
				mav.addObject("user",newuser);
				udao.saveUser(newuser);
				System.out.println("user saved");
				
			
	return mav;
		}
		
	}
	

	

	
	
	
	
	
	
}
	
	
	
	