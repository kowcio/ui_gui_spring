package ui.controllers;

import java.util.List;
import java.util.Random;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ui.entities.User;
import ui.hibernate.HibGetDBSession;
import ui.hibernate.UserDAO;
/**
 *  UTILIIES CLASS
 */
@Controller
public class Utils {

	 /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString( int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      //System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	    
	    
	   /**
	    * Method for casting Long to INT
	    * @param l long
	    * @return int l
	    */
	   public int safeLongToInt(long l) {
	       if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	           throw new IllegalArgumentException
	               (l + " cannot be cast to int without changing its value.");
	       }
	       return (int) l;
	   }


	   
	   
	   /**
	    * FILL UP THE BASE WITH USER ENTITIES
	    */
	 	@RequestMapping(value="/populatedb",method = RequestMethod.GET)
		public ModelAndView populatedb(
				ModelAndView mav,
				UserDAO fdao,
				User us
				) {
HibGetDBSession hibs = new HibGetDBSession();
SessionFactory sf = hibs.getAnnotatedSessionFactorysWithSpecificHibCfgXmlFile("hibernate.cfg.xml");

	for (int i=0 ; i < 32;i++){
			fdao.saveUser(us.createRandomFreelancer(), 	sf.openSession());
			fdao.saveUser(us.createRandomEmployer(), 	sf.openSession());
	}    		
	
	return mav;
	}	
		
	   
	   
	   
	   
	   
		/**
		 * Function showing all the errors from BindingResult object.
		 * @param result
		 */
		public void showErrorsForBRObject(BindingResult result ){
			List<ObjectError> errorList = result.getAllErrors();
			for (int i=0 ; i< errorList.size() ; i++){
				System.out.println(errorList.get(i));
			}
			
		}
	   
	   


}
