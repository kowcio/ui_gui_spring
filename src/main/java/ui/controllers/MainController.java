package ui.controllers;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ui.entities.User;
import ui.hibernate.UserDAO;

import com.google.common.annotations.VisibleForTesting;

/**
 * Main controller for logging info.
 * tu sie dodaje posty
 * tu jest lista postów
 * 
 * @author TalentLab1
 *
 */

@SuppressWarnings("unused")
@Controller
public class MainController {

	Logger logger = Logger.getLogger("uiLogger");
	
	/**
	 * 	MAIN INDEX OF THE SITE 
	 * 
	 * @param user - form from spring security 
	 * @param principal -  form from spring security
	 * @param result 
	 * @param mav 
	 * @return View of the main site.
	 */
	@RequestMapping(value={"/",""},method= RequestMethod.GET)
	public ModelAndView indexWithoutLogin(
			ModelAndView mav,
			UserDAO udao
			)
	{
		System.out.println(" Loading index.jsf");
		//load posts list anyway
		mav.addObject("lastusers", udao.getUsersNumberInDB() );

		List<User> pl = udao.getAllUsersBy30ForPaginationAjaxRequest(0,30);
		if (pl  == null)				mav.addObject("usersListEmpty" , "Users list is empty");
		else 							mav.addObject("usersList" , pl );

       mav.setViewName("index");
				//logging user only once
			  		
	    
	     return mav;
	} 
	
	

	
	
	
	
	
	//METHODS AND SHORT CODES 
		
	
	/**
	 * Add posts list  "PostsList" to the given ModelAndView object and returns it.
	 * 
	 * @param mav
	 * @return mav   mav.addObject("PostsList" , pl );
	 */
	public ModelAndView addUserList(ModelAndView mav){
		UserDAO udao = new UserDAO();
		List<User> pl = udao.getAllUsers();
		if (pl  == null || pl.isEmpty())				mav.addObject("postEMPTY" , "postListIsNull");
		else 							mav.addObject("PostsList" , pl );
		return mav;
	}

	
	
	
	

	//setters and getters
	

	public Logger getLogger() {
		return logger;
	}


	public void setLogger(Logger logger) {
		this.logger = logger;
	}


	
	

	}
	
	
