package ui.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ui.controllers.Utils;
import ui.entities.User;


public class UserDAO {

	HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	String hibfile = "hibernate.cfg.xml"; 

	
	/**
	 *Save a User  class  into the User  table
	 *
	 * @param  User 
	 * @return 
	 */
	
	public String saveUser(User User){
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
	   		Transaction tx = session.beginTransaction();
			session.save(User);
			tx.commit();
			return "OK";
		} catch (Exception e) {
			System.out.println("Error saving User "+e.getCause());
			 e.printStackTrace();
			 return "notOK";
		}
	}   	
	
	/**
	 * Method used in DBConfig for saving the Post object with provided opened session (taken from sessionFactory)
	 * @param post
	 * @param sessionProvided
	 * @return "OK" or "NotOK"
	 */
	
	public int saveUser(User user, Session sessionProvided){
	   	try {
	   		Session session = sessionProvided;
			Transaction tx = session.beginTransaction();
			session.save(user);
			System.out.println("session save user. "+user.getId());
			tx.commit();
			return user.getId();
		} catch (Exception e) {
			System.out.println("Error saving single user - ui.userDAO.saveUser. ");
			 e.printStackTrace();
				return 0;
		}

	}
	
	
	
	/**
	 *Delete a User  class from  the User  table
	 *
	 * @param  User  
	 * @return OK / notOK String
	 */
	
	public String deleteUser(User User){
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
	   		Transaction tx = session.beginTransaction();
			User.toString();
			session.delete(User);
			tx.commit();
			return "OK";
		} catch (Exception e) {
			System.out.println("Error deleting User "+e.getCause());
			 e.printStackTrace();
			 return "notOK";
		}
	}
	
	/**
	 * 
	 * @return All Users as list.
	 */
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers(){
 
		List<User> UsersList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			UsersList = (List<User>)session.createQuery("from User").list();
		   	System.out.println("UsersList count = "+UsersList.size());
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - UserDAO - getAllUsers");
			 e.printStackTrace();
		}
	   	return UsersList;
	}

	
	/**
	 * Get users by name with a wildcard
	 * Wildcard works only with like 'free%'
	 * @returnGet all users by name
	 */
	@SuppressWarnings("unchecked")
	public List<User> getAllUsersByName(String name){
 
		List<User> UsersList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			UsersList = (List<User>)session.createQuery("from User where name like '"+name+"%'").list();
		   	//System.out.println("UsersList count by name = "+UsersList.size());
			tx.commit();
	   	} catch (Exception e) {
			//System.out.println("Error - > - UserDAO - getAllUsersByName");
			 e.printStackTrace();
		}
	   	return UsersList;
	}
	
	
	
	
	/**
	 * Get users by name with a wildcard
	 * Wildcard works only with like 'free%'
	 * @returnGet all users by name
	 */
	@SuppressWarnings("unchecked")
	public List<User> getAllUsersByNameAndField(String name, String field){
		List<User> UsersList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			UsersList = (List<User>)session.createQuery("from User where "+field+" like '"+name+"%'").list();
		   	System.out.println("UsersList count by name = "+UsersList.size());
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - UserDAO - getAllUsersByName");
			 e.printStackTrace();
		}
	   	return UsersList;
	}
	
	/**
	 * Return User by ID.
	 * 
	 * @param UserId - Users ID which we wish to load.
	 * @return	Specified User
	 */
	
	
	public User getUserByID(Integer UserId){
 
		User User =new User();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			User = (User) session.get(User.class, UserId);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - getUserByID");
			 e.printStackTrace();
		}
	 	System.out.println("Pobrano User, id = "+UserId);
	   	return User;
	}

/**	Update in db a User object.
 * 
 * @param User User which we wish to update. Needs to have a specified ID.
 */

	public void updateUser(User User){
 
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(User);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - getUserByID");
			 e.printStackTrace();
		}
		System.out.println("Update User = "+User.toString());
	}



	
	/**	Get User object by name
	 * 
	 * @return User by his name...
	 */
	@SuppressWarnings("rawtypes")
	public User getUserByName(String name){
 
		User User =new User();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			System.out.println(" Login to get = " + name);
			String sql = "FROM User WHERE name = '"+name+"'";
			Query query = session.createQuery(sql);
			List results = query.list();
			User = (User) results.get(0);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - UserDAO ->  getUserByID");
			 e.printStackTrace();
		}
	   	return User;
	}



		/**
		 * Load 10 posts from given number
		 * @param numser of first post
		 * @param number of posts to load
		 * @return Posts by 10 posts starting with given number.
		 */
		@SuppressWarnings("unchecked")
		public List<User> getAllUsersBy30ForPaginationAjaxRequest(
				int firstPost,
				int numberofPostsToLoad
				){
		   	System.out.println("PL_frogetAllPostsBy10ForPaginationAjaxRequest");

			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				Transaction tx = session.beginTransaction();			 
				String strQry = "from User c order by c.id desc ";
			        Query query = session.createQuery(strQry);
			        query.setFirstResult(firstPost);
			        query.setMaxResults(numberofPostsToLoad);
			    List<User> postsList = query.list();   
			   	//System.out.println("PostsList count 1 = "+postCount +"\n q.list"+query.list());
			   	tx.commit();
			   	//System.out.println("PL_fromDAO = "+postsList.size());
			   	return postsList;
		   	} catch (Exception e) {
				System.out.println("Exception error with loading ajax posts");
				 e.printStackTrace();
			}
		   	return new ArrayList<>();
		}


		/**
		 * Get posts count in db by selected viewType		</br > 
		 * 0 - notepads for all								</br>
		 * 1 - public for all 								</br>
		 * used reateCriteria and  Restrictions.eq("viewType", viewType ) </br >
		 * @return postCount (long casted to int)
		 */
		public int getUsersNumberOfUsersInDBByUserType( String userType){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
			Utils utils = new Utils();
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				//postCount 
				int postCount = 
						utils.safeLongToInt(
						(long) session.createCriteria(User.class)
						.add( Restrictions.eq("userType", userType ) )
						.setProjection(Projections.rowCount()).uniqueResult()
						);
				return postCount;
		   	} catch (Exception e) {
				System.out.println("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
	
		/**
		 * Get posts count in db by selected viewType		</br > 
		 * 0 - notepads for all								</br>
		 * 1 - public for all 								</br>
		 * used reateCriteria and  Restrictions.eq("viewType", viewType ) </br >
		 * @return postCount (long casted to int)
		 */
		public int getUsersNumberInDB( ){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
			Utils utils = new Utils();
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				//postCount 
				System.out.println("getting post count");
				int postCount = 
						utils.safeLongToInt(
						(long) session.createCriteria(User.class)
						.setProjection(Projections.rowCount()).uniqueResult()
						);
				return postCount;
		   	} catch (Exception e) {
				System.out.println("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
	

	//setters getters
	

	public HibGetDBSession getFabrykaHibernejta() {
		return fabrykaHibernejta;
	}



	public void setFabrykaHibernejta(HibGetDBSession fabrykaHibernejta) {
		this.fabrykaHibernejta = fabrykaHibernejta;
	}


	public String getHibfile() {
		return hibfile;
	}


	public void setHibfile(String hibfile) {
		this.hibfile = hibfile;
	}



	
	
	
	
	
}
