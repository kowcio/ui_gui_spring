package ui.entities;

import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.validator.constraints.Length;

import ui.controllers.Utils;

/**
 * Freelancer entity class with fields : <br />
 * @author TalentLab1
 *
 */


@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
    private int id;

	//-1 - not assigned, 0 - freelancer , 1- employer
	@Column( unique = false, nullable = false)
	private String userType;
	
	@Length(min=5, max=200, message="Notes require 5-200 characters")
	@Column(unique = false, nullable = false)
	private String notes;
	
//freelancer
	//@Length(min=5, max=20, message="Name requires 5-20 characters ")
	//@Pattern(regexp="^[A-Za-z0-9_-]{5,20}$",message="Username must match : A-Za-z, 0-9, _, -, 5-20 chars")
	@Column( unique = false, nullable = false)
	private String firstName;
	
	//@Length(min=5, max=20, message="Second name requires 5-20 characters")
	@Column( unique = false, nullable = false)
    private String secondName;

	//@Length(min=5, max=30, message="Group description requires 5-30 characters")
	@Column( unique = false, nullable = false)
	private String proffesionalGroup;
    
//employer 
	//@Length(min=5, max=20, message="Name requires 5-20 characters")
	@Column( unique = false, nullable = false)
	private String name;
    
	@Column( unique = false, nullable = false)
	private int employeesCount;
    
	//constructors
	
    /**
     * Basic constructor with default values.
     * id = none ; 			<br />
	   name = "Name";			<br />
		secondname= "Second Name";			<br />
		userType=0;			<br />
		proffesionalGroup = "empty";			<br />
     */
	public User (){
		this.userType = 		"User type";
		this.firstName = 		"First name";
		this.secondName = 		"Second name";
		this.proffesionalGroup ="Proffesional group";
		this.name = 			"Employer name";
		this.employeesCount = 	 0;
		this.notes = 			"Notes about";
	}

	
	/**
	 * Full cstr
	 * @param id
	 * @param userType
	 * @param firstName
	 * @param secondName
	 * @param proffesionalGroup
	 * @param name
	 * @param employeesCount
	 */

	public User(int id, String userType, String firstName, String secondName,
			String proffesionalGroup, String name, int employeesCount,String notes) {
		super();
		this.id = id;
		this.userType = userType;
		this.firstName = firstName;
		this.secondName = secondName;
		this.proffesionalGroup = proffesionalGroup;
		this.name = name;
		this.employeesCount = employeesCount;
		this.notes = notes;
	}

	
	
	
	
	public User createRandomFreelancer()
	{	Random rand = new Random();
		Utils utl = new Utils();
		this.id =  rand.nextInt(1000);
		this.userType = "freelancer";
		this.notes = "notes"+utl.getRandomString(5);
		
		this.firstName = "freelancerFName"+utl.getRandomString(5);
		this.secondName = "freelancerSName"+utl.getRandomString(5);
		this.proffesionalGroup ="freelancerProffGroup"+utl.getRandomString(5);
		
		
		this.name = "freelancer";
		this.employeesCount = 0;
		return this;
	}
	
	public User createRandomEmployer()
	{   Random rand = new Random();
		Utils utl = new Utils();
		this.id = rand.nextInt(1000);
		this.userType = "employer";
		this.name = "employer"+utl.getRandomString(5);
		this.employeesCount = rand.nextInt(100);
		//clear freelancer info
		this.firstName = "employer";
		this.secondName = "employer";
		this.proffesionalGroup ="employer";
		this.notes = "notes"+utl.getRandomString(5);

		return this;
	}
	


	
	//methods 
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name
				+ ", secondname=" + secondName + ", proffesionalGroup="
				+ proffesionalGroup + "notes = "+ notes+"";
	}



    //getters setters 

    

	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}






	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getSecondname() {
		return secondName;
	}




	public void setSecondname(String secondname) {
		this.secondName = secondname;
	}




	public String getProffesionalGroup() {
		return proffesionalGroup;
	}




	public void setProffesionalGroup(String proffesionalGroup) {
		this.proffesionalGroup = proffesionalGroup;
	}


	public String getFirstname() {
		return firstName;
	}


	public void setFirstname(String firstname) {
		this.firstName = firstname;
	}


	public int getEmployeesCount() {
		return employeesCount;
	}


	public void setEmployeesCount(int employeesCount) {
		this.employeesCount = employeesCount;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSecondName() {
		return secondName;
	}


	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}



	
	
}
	