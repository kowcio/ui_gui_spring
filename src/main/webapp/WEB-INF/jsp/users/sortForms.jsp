<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


    <div id="filter_form">
        <form name="contact" method="POST" action="${path}">  
    	<input name="filterQuery" id="filterQuery" type="text">  
		<select name="field" id="field" class="">
				<option value="id">User ID</option>
				<option value="firstname">First Name</option>
				<option value="secondname">Second Name</option>
				<option value="proffesionalgroup">Proffesional Group</option>
				<option value="name">name</option>
				<option value="employesscount">Employess Count</option>
				<option value="usertype">User Type</option>
				<option value="notes">Notes</option>
		</select>
		<br />
         	<input type="submit" value="Send " name="submitFilterQuery" id="submitFilterQuery" class="btn btn-primary">
         	<p class="success allOK" id="queryok" 	style="display:none">Query has been sent successfully.</p>
  		 	<p class="error" id="querynotok"		style="display:none">Query cannot be empty </p>
    	</form>  
    </div>  
  
   	<script type="text/javascript">
 
    </script>
	
	
	





