<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>
	
	 
<!--  PAGINATION -->  
    <div class="pagination text-center">
    <ul>
    <li><a  class="pageBar"   href="#" onclick="loadFirstAjaxusers()" id="firstPage"> First   </a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxusers(0)"     id="page1">1</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxusers(10)"    id="page2">2</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxusers(20)"    id="page3">3</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxusers(30)"    id="page4">4</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxusers(40)"    id="page5">5</a></li>
    <li><a  class="pageBar"   href="#" onclick="loadLastAjaxusers(${lastusers})"  id="lastPage">  
    	<spring:message code="menu.last" text="Last" /> 30 <spring:message code="menu.from" text="from"/> <span id="usersCount" style="visibility:none"> ${lastusers}</span></a>
	</li>
    </ul>
    </div>
	<c:if test="${userEMPTY=='userEMPTY'}">${userEMPTY}</c:if> 
	
	
	
	
<script type="text/javascript">

</script>



<!--  just put here a user list  and the div will change-->

<div id="displayusers" class="displayusers">

   
<jsp:include page="/WEB-INF/jsp/users/showajaxList.jsp" />
    
 	
</div>

