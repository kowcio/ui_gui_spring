<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
    
    
    
<table class="UsersTable table table-striped table-hover">
	
				<tr class="info">
			 	<td>	ID 					</td>
			 	<td> 	FirstName			</td>
			 	<td>	SecondName  		</td>
			 	<td> 	ProffesionalGroup 	</td>
			 	<td> 	Name 				</td>
			 	<td> 	EmployeesCount	 	</td>
			 	<td> 	UserType		 	</td>
			 	<td> 	Notes			 	</td>
			 	</tr>
	<c:forEach var="user" items="${usersList}">
				<tr>
			 	<td class="userIdCell">		${user.id} 					</td>
			 	<td class="userFnCell"> 	${user.firstname}			</td>
			 	<td class="userSnCell">		${user.secondname}  		</td>
			 	<td class="userpGCell"> 	${user.proffesionalGroup} 	</td>
			 	<td class="userNaCell"> 	${user.name} 				</td>
			 	<td class="userEcCell"> 	${user.employeesCount} 		</td>
			 	<td class="useruTCell"> 	${user.userType}	 		</td>
			 	<td class="userNoCell"> 	${user.notes}		 		</td>
			 	
			 	</tr>	
	</c:forEach>
</table>		 	 	
		

    