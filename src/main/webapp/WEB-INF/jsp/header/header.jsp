
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>



<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>



<a id="glowna_header" href="<c:url value="/"/>">	<spring:message code="menu.mainpage.link" text="main page" /> 	</a><br /> 

	
<div class="ajaxBusyIndicator" id="ajaxBusyIndicator" >
	<img src="${path}/resources/img/ajax-loader.gif" 
	class="ajaxBusyIndicatorImg" width="25" height="25" alt="Ajax is on" style="display:none"/>
</div>	

