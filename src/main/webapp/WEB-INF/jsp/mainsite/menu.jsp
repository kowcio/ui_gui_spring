<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>
<div id="pageContext">${path}</div>

<!--  MAIN SITE  -->
<a id="glowna" href="<c:url value="/"/>"><spring:message code="menu.mainpage.link" text="ERROR no menu.mainpage.link code" /></a>




<jsp:include page="/WEB-INF/jsp/users/sortForms.jsp" />




    <!-- Button to trigger modal -->
    <a id="showUserWizard" href="#myModal" class="btn btn-primary" data-toggle="modal">Add user</a>
    
    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1">
    <div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal">×</button>
    	<h3 id="myModalLabel">Add user wizard</h3>
    </div>
    <div class="modal-body" id="userWizardModalBody">    </div>
    <div class="modal-footer">							   </div>
    </div>
<script type="text/javascript">
</script>



