<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>
 	 
 	  

Step : <div id="stepDescription" class="stepDescription" style="display:inline"></div>
<!-- usertype value -->
<div id="userTypeValue" class="userTypeValue"  style="display:none"></div>

<h5 id="status"></h5>
			<form:form id="registerUserFormWrapper" method="post" action="${path}/registerUser/" class="" modelAttribute="user">
				<div id="fieldWrapper">
				<div class="firstStepForm" id="firstStepForm" style="display:block">
					<span class="">Freelancer or Employer ?</span><br />
					<fieldset>	
						<label class="radio formClickFreelancer userType" id="formClickFreelancer">Freelancer
						<input type="radio" Name="userType" value="freelancer" id="formClickFreelancer"/>
						</label>
						<label class="radio formClickEmployer userType">Employer
						<input type="radio" Name="userType" value="employer" id="formClickEmployer"/>
						</label>
					</fieldset>
					</div>
					
<!--  First step load another -->					
<script type="text/javascript">
					
</script>

					
					<div id="freelancerStepForm" class="freelancerStepForm" style="display:none">
					<fieldset>
						<span class="">Welcome Freelancer</span><br />
						<form:label for="firstName" path="firstName">First Name</form:label>
						<form:input class="" Name="firstName" id="firstName" path="firstName" value=""  placeholder="First name"
						onfocus="if(this.value == 'First name') { this.value = ''; }"/>
						<div class="error firstNameErrorField" id="firstNameErrorField"></div>
						
						<form:label for="secondName"  path="secondName">Second Name</form:label>
						<form:input class="" Name="secondName" id="secondName" path="secondName" value="" 
						placeholder="Second name" onfocus="if(this.value == 'Second name') { this.value = ''; }"/>
						<div class="error" id="secondNameErrorField"></div>
						
						<form:label for="proffesionalGroup"  path="proffesionalGroup">Proffesional Group</form:label>
						<form:input class="" Name="proffesionalGroup" id="proffesionalGroup" path="proffesionalGroup" value="" 
						placeholder="Proffesional Group" onfocus="if(this.value == 'Proffesional group') { this.value = ''; }"/>
						<div class="error" id="proffesionalGroupErrorField"></div>
					</fieldset>								
					</div>
					
					<div id="employerStepForm" class="" style="display:none">
					<fieldset>
						<span class="">Welcome Employer</span><br />
						<form:label for="Name" path="Name">First Name</form:label><br />
						<form:input class="" Name="Name" id="Name" value=""  path="Name" 
						placeholder="Employer name" onfocus="if(this.value == 'Employer name') { this.value = ''; }"/>
						<div class="error" id="nameErrorField"></div>
				
						<form:label for="employeesCount"  path="employeesCount">Employees Count</form:label><br />
						<form:input class="" Name="employeesCount" id="employeesCount" path="employeesCount" value="" 
						placeholder="0" onfocus="if(this.value == '0') { this.value = ''; }"/>
						<div class="error" id="employeesCountErrorField"></div>
						 
					</fieldset>								
					</div>
					
					<div id="secondStepForm" class="secondStepForm" style="display:none">  
						<fieldset>
						<form:label for="notes" path="notes">Notes</form:label><br />
                  		<form:textarea path="notes" class="" rows="5" cols="450"
                  		placeholder="Notes" onfocus="if(this.value == 'Notes about') { this.value = ''; }"/> 
                  		<div class="error" id="notesErrorField"></div>
	       		        </fieldset>								
					</div>
				</div>
				
				<div id="formNavigation"> 							
					<a href="#" class="navigation_button backRegUserBtn btn" id="backRegUserBtn" style="display:none">Back</a>
					<a href="#" class="navigation_button nextRegUserBtn btn" id="nextRegUserBtn" style="display:none">Next</a> 
					<input type="submit" value="Save user" id="saveUserBtn" class="btn btn-success" style="display:none"/>			
				</div>
				
<!--  describe button behaviour depending on whole form -->		
<script type="text/javascript">



/**
* Validate notes step
*/
function validateNotesStep(){
				var isValid = true;
				var n = $('#notes').val();
				if ( n.length < 5  ) {
					$('#notesErrorField').html("Write some note");
					isValid = false;
				}else  $('#notesErrorField').html("");
				return isValid;
				}
			

	
			
			
			
			





			
</script>
				
				
			</form:form>
			<hr />
