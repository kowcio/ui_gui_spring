/**
 * Need to have everything inside function{ here } - or not ?
 * 
 * change  "${path}  	=> 		$("#pageContext").text()+" 
 * there is the path stored by ${path} in the index
 * 
 * window.loadAjaxusers = function loadAjaxusers(postNo) - makes a function a global scope JS
 * 
 */
//$(function(){
//$(window).bind("load",function(){	
//$(document).ready(function(){
	

$(document).ready(function(){
	
	
//menu.jsp	
	$("#showUserWizard").click(function() {
		$("#userWizardModalBody").load(	$("#pageContext").text()+"/registerUser/");
	});

	
	
//sort forms   	
	
	  // Contact Form
	$("#filter_form").submit(function(e){
	          e.preventDefault();
	          var filterQuery = $("#filterQuery").val();
	          var field = $("#field").val();
	          var dataString = filterQuery+"/"+field ;

		      //constraints to add below
	          //if (isValidEmail(email) && (text.length > 100) && (name.length > 1)){
	           if (filterQuery.length > 0){ 
					            $.ajax({
					            type: "GET",
					            url:  $("#pageContext").text()+"/",
					            data: dataString,
					            success: function(){
					            	$("#displayusers").load("http://127.0.0.1:8080/"+$("#pageContext").text()+"/users/filterby/"+dataString);
					            	$('.success').fadeIn(1000);
					            	$('.success').fadeOut(1000);
					          	}
					            });
	          } //if not validated
	           else{  	$('.error').fadeIn(1000);  	
	           			$('.error').fadeOut(1000);	
	           		}
	         
	        });
	        

	
		$("#submit_btn1").click(function(){
			validateSortByName();
			ajaxFilterByField();
		  });
		
		
	   function validateSortByName() {  	//http://net.tutsplus.com/tutorials/javascript-ajax/submit-a-form-without-page-refresh-using-jquery/
	        $('.error').hide();//when we click the submit button
	        $("#submit_btn1").click(function() {// validate and process form here  
	          $('.error').hide();
	         var name = $("input#name").val();  
	         if (name == ""){  
	            $("label#name_error").show();  
	            $("input#name").focus();  
	            return false;  
	         }});};  
	    
	function ajaxFilterByField()   {
	    var frm = $('#filter_form'); var name=$('.text-input');
	    frm.submit(function () {
	        $.ajax({
	            type: frm.attr('method'),
	            url: frm.attr('action'+name),	
	            data: frm.serialize(),
	            success: function (data) { //load the new ajax userList
		        	$("#mainPagePosts").load(window.location.pathname+frm.attr('action')+name.text());
	            }
	        });

	        return false;
	    });
	}
	
	
	
	
	
// show ajax jsp
	//script for changing div with the content returned from urls
	window.loadAjaxusers = function loadAjaxusers(postNo)
	{        
		$(".displayusers").load($("#pageContext").text()+"/users/part/"+postNo);  
				 var baseValue = 30;// for making display of 10/20/30 posts and etc
		     	 var i = Math.floor(postNo/baseValue) - 2;
				 var j = baseValue * 2;
				 var basicValues=0;
			 $( ".pageBarNo" ).each(function( index ) {
					i++;
				 //for first, second and third webpage
					if (postNo <= baseValue*2){
						basicValues+=baseValue;
						$(this).attr("onclick","loadAjaxusers("+ (basicValues-baseValue)	 +")");
						$(this).text(basicValues/baseValue);
					}
					//if (postNo > baseValue){
					else{
						$(this).attr("onclick","loadAjaxusers("+ ( postNo - j)	 +")");
					 	j-=baseValue;
				 	 	//var onClick =  $(this).attr("onclick");				
					 	//alert( 	onClick		+" check div = "	);
						$(this).text(i);
				 	}
				//console.log( index + ": " + $(this).text() 	);
			 });
	};   
	window.loadLastAjaxusers =function loadLastAjaxusers(postNo)
	{			
				var baseValue = 30;// for making display of 10/20/30 posts and etc
				postNo = Math.floor(postNo / baseValue) * baseValue ;
				//alert(postNo);
				loadAjaxusers(postNo);              
	};   
	window.loadFirstAjaxusers =function loadFirstAjaxusers()
	{		 
				loadAjaxusers(0);              
	};       
	
	
	
	
	/*******************************
	********************************
	
	registerUserForm.jsp javascript for the wizard
	
	********************************
	********************************
	*/
	/*
	*	When we click the Freelancer option for the user
	*/
	//	$(".formClickFreelancer").click(function() {
	//	$('.formClickFreelancer').live('click', function() {

	//this is for jquer 2.0 above depraceted etc... and the rest works fine ? 
	$('body').on('click', '.formClickFreelancer', function(){
	
							$('#userTypeValue').html("freelancer");
							$('#firstStepForm').hide(1000);
							$('#freelancerStepForm').show(1000);
							$('#stepDescription').html( "Freelancer information" );
							$('.nextRegUserBtn').show(1000);
	});
	/*
	*	When we click the Employer option for the user
	*/					
	$('body').on('click', '.formClickEmployer', function(){
							$('#userTypeValue').html("employer");
							$('#firstStepForm').hide(1000);
							$('#employerStepForm').show(1000);
							$('#stepDescription').html( "Employer information" );
							$('.nextRegUserBtn').show(1000);
							});
	
	
	/*
	*	When we click the back button - and the rest part
	*/
	//$("#backRegUserBtn").click(function() {
	$('body').on('click', '.backRegUserBtn', function(){
		
		//clear the fields from errors if the user clicked the form again
		$('#firstNameErrorField').html("");
		$('#secondNameErrorField').html("");
		$('#proffesionalGroupErrorField').html("");
		$('#nameErrorField').html("");
		$('#employeesCountErrorField').html("");
		$('#notesErrorField').html("");

		
	var whichUser = 	$('#userTypeValue').text();

	if (   whichUser == "freelancer"  ){
						console.log("freelancer back loading");
						//form loading
						$('#secondStepForm').hide(1000);
						$('#freelancerStepForm').show(1000);
						//description step
						$('#stepDescription').html( "Freelancer information" );
						//button fades out
						$('#backRegUserBtn').hide();
						$('#saveUserBtn').hide();
						$('.nextRegUserBtn').show();
					}	
					else if (   whichUser == "employer"  ){
						console.log("employer back loading");
						$('#secondStepForm').hide(1000);
						$('#employerStepForm').show(1000);
						$('#stepDescription').html( "Employer information" );
						$('#backRegUserBtn').hide();
						$('#saveUserBtn').hide();
						$('.nextRegUserBtn').show();
					}
					else  {
						document.location.reload(true);
					}
					});
	
	
	
	/*
	* When we click the next button 
	*/
	//$(".nextRegUserBtn").click(function() {
	$('body').on('click', '.nextRegUserBtn', function(){
				//load the first step for the freelancer				
					if (   $('#stepDescription').text() == 'Freelancer information'  ){
						//validation
						if (validateFirstStepFreelancer()==false) return false;
								//form
								$('.freelancerStepForm').hide(1000);
								$('.secondStepForm').show(1000);
								//descritpion
								$('#stepDescription').html( "Notes about" );
								//buttons
								$('.backRegUserBtn').show();
								$('.nextRegUserBtn').hide();
								$('#saveUserBtn').show();
					}
			//load the first step for the employer				
					if (   $('#stepDescription').text() == 'Employer information'  ){
						//validation
						if (validateFirstStepEmployer()==false) return false;
								//form
								$('#employerStepForm').hide(1000);
								$('.secondStepForm').show(1000);
								//step description
								$('#stepDescription').html( "Notes about" );
								//buttons
								$('.backRegUserBtn').show();
								$('.nextRegUserBtn').hide();
								$('#saveUserBtn').show();
					}
				});				
			
	
	
	
	

	
	/**
	* 	T ODO `s when we submit the form.
	*/
	//$("#saveUserBtn").click(function() {
	$('body').on('click', '#saveUserBtn', function(){

	if (   $('#stepDescription').text() == 'Notes about'  ){
					//validation
					if (validateNotesStep()==false) return false;
					changeUserNotUsedFields();
					//hide the modal
					
					//attempt to ajax post the values
					var data = $("#registerUserFormWrapper").serialize();
					//post the user
				    $.ajax({
				    	type: "POST",
				    	url: $("#pageContext").text()+"/registerUser/",
				    	data: data,
				    	success: function(){
				    	       alert("Success adding a user.");
				    	    }
				    	});
				    //load the list with the new user -> 1 req less !! 
				    // data is the returned html !!
					 $.ajax({
				            type: "GET",
				            url:  $("#pageContext").text()+"/users/part/0",
				            success: function(data){
				            	$("#displayusers").html(data);
				            	$('.success').fadeIn(1000);
				            	$('.success').fadeOut(1000);
				          	}
				            });
				    
				    $('#myModal').modal('hide');

					//return false so we do an ajax instead of click and submit with reload
					return false;
				}
	});
	
	
	
	//	window.loadFirstAjaxusers =function loadFirstAjaxusers()

	/**
	* change user fields to "-" if they are not used 
	*/
	window.changeUserNotUsedFields = function changeUserNotUsedFields(){
					var whichUser = 	$('#userTypeValue').text();
					if (   whichUser == "freelancer"  ){
						//set values for not used fields
						$('#Name').val("-");
						$('#employeesCount').val("0");
					}
					else if (whichUser == "employer"){
					//set values for not used fields
					$('#firstName').val("-");
					$('#secondName').val("-");
					$('#proffesionalGroup').val("-");
					}
					else{	document.location.reload(true);
	}
};
	
/*
*	Validate first step employer form
*/
window.validateFirstStepEmployer = 	function validateFirstStepEmployer(){
				var isValid = true;
				var n = $('#Name').val();
				var eC = $('#employeesCount').val();
				if ( n.length < 5  ) {
					$('#nameErrorField').html("Name should be at least 5 characters long");
					isValid = false;
				}else  $('#nameErrorField').html("");
				if ( eC < 0  ) {
					$('#employeesCountErrorField').html("Employees count can`t be negative");
					isValid = false;
				}else  $('#employeesCountErrorField').html("");	
				return isValid;
				};
	
				
				/*
				*	Validate first step freelancer form
				*/
window.validateFirstStepFreelancer = 	function validateFirstStepFreelancer(){
								var isValid = true;
								var fN = $('#firstName').val();
								var sN = $('#secondName').val();
								var pG = $('#proffesionalGroup').val();
								
								if ( fN.length < 5  ) {
									$('#firstNameErrorField').html("First name should be at least 5 characters long");
									isValid = false;
								}else  $('#firstNameErrorField').html("");	
								if (  sN.length < 5   ) {
									$('#secondNameErrorField').html("Second name should be at least 5 characters long");
									isValid = false;
								}else  $('#secondNameErrorField').html("");	
								if (  pG.length < 5 ) {
									$('#proffesionalGroupErrorField').html("Professional group name should be at least 5 characters long");
									isValid = false;
								}else  $('#proffesionalGroupErrorField').html("");	
								
								return isValid;
								};
							
	
	/**
	 * For the pretty ajax loader info
	 */

$(document).ajaxStart(function() {
        // show image here
        $('.ajaxBusyIndicatorImg').fadeIn( 1000);
        //						.show('pulsate',{}, 1000);
    });
$(document).ajaxStop(function() {
    // show image here
    $('.ajaxBusyIndicatorImg').fadeOut(1000);
});
	

	
	
	//end of docuemnt ready
});






