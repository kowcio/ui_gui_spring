package ui.Tests;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
//Let's import Mockito statically so that the code looks clearer


@RunWith(MockitoJUnitRunner.class)
public class SeleniumTest {

	String context = "";
	int waitTime 		=  250;					//ms for input fields 
	String timeout = "1500";				//ms
	String addurl = "http://localhost:8080"+context+"/";
	String indexurl = "http://localhost:8080"+context;
	
	
	
/**
 * Test for filtering the list with users
 * - problem with not loading ajaxrequest by the selenium
 */
	@Test
	public void testFilteringTheList() {

    WebDriver driver = new FirefoxDriver();
    driver.get(indexurl);
    WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

//input the query - change 3 to random
   sel.type("filterQuery" , "3");
   
//select the ID fomr the drop down menu
   
   WebElement select = driver.findElement(By.id("field"));
   Select dropDown = new Select(select);  
   dropDown.selectByValue("id");
   
   sel.click("submitFilterQuery");
   
   sel.waitForPageToLoad("2000");
   
   List<WebElement> filteredIds = driver.findElements(By.className("userIdCell"));
   for ( WebElement we : filteredIds) {
	   char[] str = we.getText().toCharArray();
		Assert.assertEquals("First part ofthe query and ids returned " , str[0] , "3" );

	   
   }
   

   driver.close();
   driver.quit();



}
	
	
	
	
	/**
	 * Test for adding a new user 
	 */
	@Test
	public void testAddingUser() {

    WebDriver driver = new FirefoxDriver();
    driver.get(indexurl);
    WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);
//input the query - change 3 to random
   sel.click("showUserWizard");
   sel.waitForPageToLoad(timeout);

// click the freelancr option
   WebElement freelancerStepClick = driver.findElement(By.id("formClickFreelancer"));
   freelancerStepClick.click();
   sel.waitForPageToLoad(timeout);

//fill the freelancer info
   WebElement element = driver.findElement(By.id("firstName"));
   String freelancerName = "MyFreeLancerName"+getRandomString(5);
   element.sendKeys(freelancerName);   
   WebElement element2 = driver.findElement(By.id("secondName"));
   element2.sendKeys("MyFreeLancerSecondName");   
   WebElement element3 = driver.findElement(By.id("proffesionalGroup"));
   element3.sendKeys("MyFreelancrProffesionalGroup");   
   
   clickElementByIdOrClass(driver, "nextRegUserBtn", "id");
   sel.waitForPageToLoad(timeout);

   inputFormData(driver, "notes"," noteas about the freelancer and etc.");
   clickElementByIdOrClass(driver, "saveUserBtn", "id");

   sel.waitForPageToLoad("2000");
   
   
   WebElement firstTopUserName = driver.findElement(By.className("userFnCell"));
   sel.waitForPageToLoad("2000");

   
   
   Assert.assertEquals("Check the name ",freelancerName ,firstTopUserName.getText() );
   
   driver.close();
   driver.quit();
  
}
	
	
	
	
	
	
	
	
	
	
	
	// UTILS and short codes.
	
/**
 * Get element by ID and input data
 */
	public void inputFormData(WebDriver driver, String fieldId, String data){
		 WebElement element = driver.findElement(By.id(fieldId));
		   element.sendKeys(data);   
	}
	
	/**
	 * Click element by ID or Clas <br />
	 * specify "id" or "class" for the selector
	 */
		public void clickElementByIdOrClass(WebDriver driver, String fieldName, String classID){
			if ( classID.equals("id")){
			 WebElement element = driver.findElement(By.id(fieldName));
			 element.click();
			}
			else if (classID.equals("class") ){
				 WebElement element = driver.findElement(By.className(fieldName));
				 element.click();
			}
			 
			    
		}
		
	
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   
	   

}
