package ui.Tests;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.web.servlet.ModelAndView;

import ui.controllers.UserAjaxController;
import ui.entities.User;
import ui.hibernate.UserDAO;
//Let's import Mockito statically so that the code looks clearer


@RunWith(MockitoJUnitRunner.class)
public class MockitoAjaxControllerTest {

	/**
	 * 	Mockito frame for developing tests		<br />
	 * 
	 */
	@Mock	ModelAndView mockMav = new ModelAndView();
	@Mock   UserDAO mockUDao;
	String mockString = "mocked test string";

	@SuppressWarnings("unchecked")
	@Test
 	public void checkgettingTheUsersFromASpecifiedNumber(){
		UserAjaxController uac = new UserAjaxController();
		//given
		List<User> ul = new ArrayList<>();
		List<User> ulEmpty = new ArrayList<>();
		
//when we have the full list with 30 users		
		for (int i=0 ; i<30 ; i++){
			ul.add(new User().createRandomFreelancer());
		}
		
		when(mockUDao.
		getAllUsersBy30ForPaginationAjaxRequest( anyInt() , eq(30) ) )
		.thenReturn(ul);	
		//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = uac.show30usersStartingFromFirst(0 , 
				new ModelAndView(), mockUDao);		//
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
		List<User> ulRes = (List<User>) result.getModel().get("usersList");
		Assert.assertEquals("If there are 30 users in the list", 
				ulRes.size() , 30);
		
		
//if list is empty
		when(mockUDao.
		getAllUsersBy30ForPaginationAjaxRequest( anyInt() , eq(30) ) )
		.thenReturn(ulEmpty);
		//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result2 = uac.show30usersStartingFromFirst(0 ,new ModelAndView(), mockUDao);		//
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
		Assert.assertEquals("If there are 30 users in the list", 
				 result2.getModel().get("userEMPTY") ,"userEMPTY");		
		
		
//if list is null
		when(mockUDao.
		getAllUsersBy30ForPaginationAjaxRequest( anyInt() , eq(30) ) )
		.thenReturn(null);
		//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result3 = uac.show30usersStartingFromFirst(0 ,new ModelAndView(), mockUDao);		//
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
		Assert.assertEquals("If there are 30 users in the list", 
				 result3.getModel().get("userEMPTY") ,"userEMPTY");		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// UTILS and short codes.
	


	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   
	   

}
